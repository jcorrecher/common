-- * header
-------------------------------------------------------------------------------
-- Title      : function package
-- Project    : common
-------------------------------------------------------------------------------
-- File       : function_pkg.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-05-27
-- Last update: 2021-08-31
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This package contains VHDL generic functions that can be used
--              in different rtl projects.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-27  1.0      jcorrecher Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * package declaration
-------------------------------------------------------------------------------

package function_pkg is

  function f_b2g (signal din : unsigned) return unsigned;
  function f_g2b (signal din : unsigned) return unsigned;
  function f_zxt (arg  : unsigned;
                  size : integer) return unsigned;
  function f_mlen (len_a : natural;
                   len_b : natural) return natural;
  function f_crosswalk (constant len : natural;
                        constant lsb : boolean) return unsigned;

end package function_pkg;

-------------------------------------------------------------------------------
-- * package body
-------------------------------------------------------------------------------

package body function_pkg is

-------------------------------------------------------------------------------
-- * conversion
-------------------------------------------------------------------------------

  -- ** convert binary to gray
  function f_b2g (signal din : unsigned) return unsigned is
    variable bin2g : unsigned(din'range);
  begin  -- b2g
    bin2g(din'high) := din(din'high);
    bin2gray :
    for i in 0 to din'length-2 loop
      bin2g(i) := din(i+1) xor din(i);
    end loop;
    return (bin2g);
  end f_b2g;

  -- ** convert gray to binary
  function f_g2b (signal din : unsigned) return unsigned is
    variable g2bin : unsigned(din'range);
  begin  -- g2b
    g2bin(din'high) := din(din'high);
    gray2bin :
    for i in din'length-2 downto 0 loop
      g2bin(i) := g2bin(i+1) xor din(i);
    end loop;
    return (g2bin);
  end f_g2b;

  -- ** right zero extension
  function f_zxt (arg  : unsigned;
                  size : integer) return unsigned is
    variable result : unsigned(size-1 downto 0) := (others => '0');
  begin
    -- result(arg'range) := unsigned(arg);                -- option a.
    result := resize(unsigned(arg), size);  -- option b.
    result := shift_left(result, size-arg'length);
    return unsigned(result);
  end f_zxt;

  -- ** find max length
  function f_mlen (len_a : natural;
                   len_b : natural) return natural is
  begin
    if (len_a > len_b) then
      return len_a;
    else
      return len_b;
    end if;
  end f_mlen;

-------------------------------------------------------------------------------
-- * data creation
-------------------------------------------------------------------------------

  function f_crosswalk (constant len : natural;
                        constant lsb : boolean) return unsigned is
    variable result : unsigned(len-1 downto 0) := (others => '0');
    variable bit    : std_logic := '0';

  begin
    -- select initial value for LSB
    if lsb = true then
      bit := '0';
    else
      bit := '1';
    end if;
    -- create alternated 0 and 1 word
    for i in 0 to len-1 loop
      result(i) := bit;
      bit := not bit;
    end loop;
    return result;
  end f_crosswalk;

end package body function_pkg;
