#! /usr/bin/env bash

# * Setup scripts

# source code directory

if [ $# == 1 ]; then
    SRCDIR=$1
else
    # download source code
    SRCDIR=/opt/HDL/osvvm/OsvvmLibraries
    [ -e $SRCDIR ] && rm -rf $SRCDIR
    # git clone --recursive https://github.com/OSVVM/OsvvmLibraries.git $SRCDIR
      git clone https://github.com/OSVVM/OsvvmLibraries.git $SRCDIR
      git -C $SRCDIR checkout 2023.09a
      git -C $SRCDIR submodule init
      git -C $SRCDIR submodule update

fi

# output directory

OUTDIR=/opt/HDL/osvvm/CompiledLibraries
[ -e $OUTDIR ] && rm -rf $OUTDIR

# create temporary directory

tmp_dir=$(mktemp -d -t XXXXXXXXXX)
[ -e $tmp_dir ] && rm -rf $tmp_dir
mkdir -p $tmp_dir/scripts

# copy scripts

if [[ $OSTYPE == "msys" ]]; then
    GHDL_LIB_DIR=/mingw64/lib/ghdl
    GHDL_BIN_DIR=/mingw64/bin/ghdl
else
    #elif [[ $OSTYPE == "linux_gnu" ]]; then
    GHDL_LIB_DIR=/usr/lib/ghdl
    GHDL_BIN_DIR=/usr/bin/ghdl
fi

cp $GHDL_LIB_DIR/ansi_color.sh $tmp_dir/.
cp $GHDL_LIB_DIR/vendors/* $tmp_dir/scripts/.
cp *.sh $tmp_dir/scripts/.

# * compile

# ** OsvvmLibraries osvvm

# use ghdl provided default script
$tmp_dir/scripts/compile-osvvm.sh \
        --all \
        --output $OUTDIR \
        --source $SRCDIR/osvvm \
        --ghdl $GHDL_BIN_DIR

# ** OsvvmLibraries common

# use customized, local script
$tmp_dir/scripts/compile-osvvm-common.sh \
        --all \
        --output $OUTDIR \
        --source $SRCDIR/Common \
        --ghdl $GHDL_BIN_DIR

# ** OsvvmLibraries uart

# use customized, local script
$tmp_dir/scripts/compile-osvvm-uart.sh \
        --all \
        --output $OUTDIR \
        --source $SRCDIR/UART \
        --ghdl $GHDL_BIN_DIR

# ** OsvvmLibraries dpram

# use customized, local script
$tmp_dir/scripts/compile-osvvm-dpram.sh \
        --all \
        --output $OUTDIR \
        --source $SRCDIR/DpRam \
        --ghdl $GHDL_BIN_DIR

# ** OsvvmLibraries axi4

# use customized, local script
 $tmp_dir/scripts/compile-osvvm-axi4.sh \
         --all \
         --output $OUTDIR \
         --source $SRCDIR/AXI4 \
         --ghdl $GHDL_BIN_DIR         
         
# ** OsvvmLibraries ethernet

# use customized, local script
$tmp_dir/scripts/compile-osvvm-ethernet.sh \
    --all \
    --output $OUTDIR \
    --source $SRCDIR/Ethernet \
    --ghdl $GHDL_BIN_DIR

# ** OsvvmLibraries cosim

# use customized, local script
$tmp_dir/scripts/compile-osvvm-cosim.sh \
    --all \
    --output $OUTDIR \
    --source $SRCDIR/CoSim \
    --ghdl $GHDL_BIN_DIR  

# * Cleanup

[ -e tmp_dir ] && rm -rf tmp_dir

# sudo chmod -R g+w $OUTDIR
# sudo chown -R root:ghdl $OUTDIR
